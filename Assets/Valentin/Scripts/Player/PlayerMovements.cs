﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using DG.Tweening;

public class PlayerMovements : MonoBehaviour
{
    public Animator animator;

    public float moveHorizontal;
    public float speed = 10f;
    public float rayDistance = 2.5f;

    public float jumpForce = 10f;
    public float fallMultiplier = 1f;
    public float lowJumpMultiplier = 2f;

    public bool isFliped = false;

    public LayerMask layers;

    public bool isGrounded = false;
    public bool isOnSideWall = false;
    public bool isOnSideWallRight = false;

    public Rigidbody2D rgbd;
    public float distanceRay;

    //KnockBack
    public float knockback = 15f;
    private float knockbackLenght;
    public float knockbackCount = 0;

    public bool knockbackRight;

    public bool knockbackpikes;
    //GameObject liés au player
    public GameObject sword;
    public float timerAttack = 1f;
    public float refreshAttack = 1f;

    // Effect de juice quand le player bouge et saute
    public ParticleSystem footeffect;
    private ParticleSystem.EmissionModule footemission;
    
    // Start is called before the first frame update
    void Start()
    {
        rgbd = gameObject.GetComponent<Rigidbody2D>();
        footemission = footeffect.emission;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D wallrightInfo = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - 0.5f),
            Vector2.right, distanceRay, layers);
        RaycastHit2D WallleftInfo = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - 0.5f),
            Vector2.left, distanceRay, layers);

        if (wallrightInfo == true)
        {
            isOnSideWallRight = true;
            animator.SetBool("iswall", true);
            animator.SetBool("isJumping", false);
            animator.SetBool("walk", false);
        }

        if (WallleftInfo == true)
        {
            isOnSideWallRight = false;
            animator.SetBool("iswall", true);
            animator.SetBool("isJumping", false);
            animator.SetBool("walk", false);
        }

        if (knockbackCount <= 0)
        {
            if (!isOnSideWall)
            {
                if (Input.anyKey)
                {
                    //Récupère l'axe de 0 à 1 de la pression du bouton ou du joystick
                    moveHorizontal = Input.GetAxis("Horizontal");
                    //Multiplication par le temps pour que le joueur se déplace à une vitesse normale
                    moveHorizontal *= Time.deltaTime;

                    //Fait bouger le personnage dans l'espace avec une vitesse donnée, si il va a gauche
                    //change la direction du sprite et inversement.
                    //Modifie aussi la valeur isFliped pour gérer la direction de l'épée
                    if (moveHorizontal < 0)
                    {
                        animator.SetBool("walk", true);
                        animator.SetBool("iswall", false);
                        isFliped = true;
                        transform.eulerAngles = new Vector3(0f, -180f, 0f);
                        transform.Translate(-moveHorizontal * speed, 0, 0);
                    }
                    else if (moveHorizontal > 0)
                    {
                        animator.SetBool("walk", true);
                        animator.SetBool("iswall", false);
                        isFliped = false;
                        transform.eulerAngles = new Vector3(0f, 0f, 0f);
                        transform.Translate(moveHorizontal * speed, 0, 0);
                    }

                    if (Physics2D.Raycast(transform.position, Vector2.down, rayDistance, layers))
                    {
                        isGrounded = true;
                    }
                    else
                    {
                        isGrounded = false;
                    }
                }

                else
                {
                    animator.SetBool("walk", false);
                    animator.SetBool("iswall", false);
                    rgbd.velocity = new Vector2(0f, rgbd.velocity.y);
                }

                if (isGrounded)
                {
                    if (Input.GetButton("JumpPlatformer"))
                    {
                        Jump();
                        animator.SetBool("isJumping", true);
                        animator.SetBool("iswall", false);
                    }

                   
                }


                //Si le joueur est en train de tomber 
                if (rgbd.velocity.y < 0)
                {
                    //Ajoute une valeur de gravité dans la chute du joueur et permet de faire une chute plus ou moins longue avec le fallMultiplier + la gravité du Rigidbody
                    rgbd.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier) * Time.deltaTime;
                    animator.SetBool("isJumping", false);
                }
                //Si le joueur est en l'air et qu'on ne touche pas le bouton de saut
                else if (rgbd.velocity.y > 0 && !Input.GetButton("JumpPlatformer"))
                {
                    //Permet de faire un petit saut ou un saut plus grand si on reste appuyé sur la barre de saut, la vitesse de la chute (lowJumpMultiplier) est moins importante
                    //que le fallMultiplier pour donner un meilleur effet de chute lors d'un petit saut
                    rgbd.velocity += Vector2.up * (Physics2D.gravity.y * (lowJumpMultiplier) * (Time.deltaTime * 1.3f));
                }
            }
            else
            {
                rgbd.velocity = new Vector2(0f, -0.2f);

                if (Input.GetButtonDown("JumpPlatformer") && (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow))
                    || Input.GetButtonDown("JumpPlatformer") && (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)))
                {
                    animator.SetBool("iswall", false);
                    SideWallJump();
                }
            }
        }
        else
        {
            if (knockbackRight)
            {
                rgbd.velocity = new Vector2(-knockback, knockback * 0.2f);
            }
            else
            {
                rgbd.velocity = new Vector2(knockback, knockback * 0.2f);
            }
                // pousse le joueur vers le haut 
            if (knockbackpikes)
            {
                rgbd.velocity = new Vector2(0, knockback * 0.8f);
            }
         

            knockbackCount -= Time.deltaTime;
        }


        if (timerAttack >= 0)
        {
            timerAttack -= Time.deltaTime;
        }

        if (timerAttack <= 0)
        {
            if (Input.GetKey(KeyCode.F))
            {   
                FindObjectOfType<Audiomanager>().Play("PlayerAttack");
                if (isFliped)
                {
                    sword.GetComponentInChildren<SwordScript>().DisplayColliderAndSprite();
                    sword.transform.DORotate(new Vector3(0, sword.transform.rotation.y, 90), 0.27f)
                        .OnComplete(() => sword.transform.DORotate(Vector3.zero, 0.27f)
                            .OnComplete(sword.GetComponentInChildren<SwordScript>().HideColliderAndSprite));
                }
                else
                {
                    sword.GetComponentInChildren<SwordScript>().DisplayColliderAndSprite();
                    sword.transform.DORotate(new Vector3(0, sword.transform.rotation.y, -90), 0.27f)
                        .OnComplete(() => sword.transform.DORotate(Vector3.zero, 0.27f)
                            .OnComplete(sword.GetComponentInChildren<SwordScript>().HideColliderAndSprite));
                }

                timerAttack = refreshAttack;
            }
        }
        // Permet de montrer l'effect de juice footeffect
        if (Input.GetAxisRaw("Horizontal") != 0 && isGrounded)
        {
            footemission.rateOverTime = 35f;
        }
        else
        {
            footemission.rateOverTime = 0f;
        }
        
    }
    

    public void Jump()
    {
        rgbd.velocity = Vector2.up * jumpForce;
    }

    public void SideWallJump()
    {
        isOnSideWall = false;
        Jump();
        if (isOnSideWallRight)
        {
            rgbd.AddForce(new Vector2(-2f, 0), ForceMode2D.Impulse);
            
        }
        else
        {
            rgbd.AddForce(new Vector2(2f, 0), ForceMode2D.Impulse);
           
        }
    }
}