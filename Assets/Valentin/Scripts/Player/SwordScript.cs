﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;

public class SwordScript : MonoBehaviour
{
    public BoxCollider2D boxCollider;
    public TimeManagerScript timeManager;
    public QTEManager qteManager;
    public PlayerMovements player;

    public monsterScript focusedMonster;
    public bool inMonster = false;
    public int chainQTE;
    
    public bool onBigSword = false;
    public float swordDmg;
    public float swordBaseDmg;
    public float bigSwordBaseDmg;




    // Start is called before the first frame update
    void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovements>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!player)
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovements>();
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            inMonster = false;
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Monster") || other.gameObject.CompareTag("Boss"))
        {
            focusedMonster = other.gameObject.GetComponent<monsterScript>();
            
            if (!inMonster)
            {
                if (focusedMonster)
                {
                    StartCoroutine(other.gameObject.GetComponent<monsterScript>().WaitForQTE());
                    timeManager.BulletTime();
                    qteManager.ActivateQTE(other.gameObject.GetComponent<monsterScript>());
                    inMonster = true;
                }
            }
        }

        if (onBigSword)
        {
            if (other.gameObject.CompareTag("FracturedWall"))
            {
                Destroy(other.gameObject);
            }
        }
    }
    
    public void DisplayColliderAndSprite()
    {
        gameObject.GetComponent<SpriteRenderer>().enabled = true;
            boxCollider.enabled = true;
    }
    
    public void HideColliderAndSprite()
    {
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        boxCollider.enabled = false;
    }

    public void ResetChainQTE()
    {
        chainQTE = 1;
        if (focusedMonster)
        {
            focusedMonster.RefreshList(3);
            chainQTE = 1;
        }
    }
    
}
    /*
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Monster") || other.gameObject.CompareTag("Boss"))
        {
            if (inMonster)
            {
                inMonster = false;
            }
        }
    }*/

