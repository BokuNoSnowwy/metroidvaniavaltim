﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PenguinBossScript : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject player;
    private float speed = 3f;
    public monsterScript stats;
    
    public bool movingRight = true;
    public float distanceBetweenPlayer;
    
    //jump
    public bool isGrounded;
    public float rayDistance;
    public LayerMask layers;

    public bool awake;
    
    //attack
    public bool onAttack = false;
    public bool onAttack1 = false;
    public bool onAttack2 = false;
    
    public float timerAttack = 6;
    public float timerAttackReset = 6;
    
    public float timerAttack2 = 0.3f;

    public float jumpForce;

    // Start is called before the first frame update
    void Start()
    {
        stats = GetComponent<monsterScript>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (Physics2D.Raycast(transform.position, Vector2.down, rayDistance, layers))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
        
        if (awake)
        {
            if (isGrounded)
            {
                //Vérifie si le monstre est proche du player
                if (System.Math.Abs(transform.position.x - player.transform.position.x) < 50f && System.Math.Abs(transform.position.x - player.transform.position.x) > distanceBetweenPlayer)
                {
                    transform.Translate(Vector2.right * speed * Time.deltaTime);

                    //Si il est proche le pourchasse
                    if (transform.position.x >= player.transform.position.x)
                        //Quand le joueur passe par dessus le monstre, ce dernier change de sens pour le suivre
                        transform.eulerAngles = new Vector3(0f, -180, 0f);
                    else
                    {
                        transform.eulerAngles = new Vector3(0f, 0, 0f);
                    }
                }
                else if (System.Math.Abs(transform.position.x - player.transform.position.x) <= distanceBetweenPlayer)
                {

                }
            }
            else
            {
                transform.Translate(Vector2.right * speed * Time.deltaTime);
            }

            //ATTACK1
            if (timerAttack >= 0)
            {
                timerAttack -= Time.deltaTime;        
            }

            if (timerAttack <= 0)
            {
                int attack = Random.Range(0, 2);
                switch (attack)
                {
                    case 0 :
                        StartCoroutine(Attack1Duration());
                        break;
                    case 1 :
                        StartCoroutine(Attack2Duration());
                        break;
                    default:
                        break;
                }
                timerAttack = timerAttackReset;
            }
            
            if (onAttack1)
            {
                if (isGrounded)
                {
                    Debug.Log("Jump");
                    GetComponent<Rigidbody2D>().velocity = Vector2.up * jumpForce;
                }
            }
                
            if (onAttack2)
            {
                    
                if (timerAttack2 >= 0)
                {
                    timerAttack2 -= Time.deltaTime;
                }

                if (timerAttack2 <= 0)
                {
                    Attack2();
                    timerAttack2 =  Random.Range(0.1f,0.15f);
                }
            }
            
            if (stats.health <= 0)
            {
                gameObject.SetActive(false);
                GetComponentInChildren<PlantBossDetectionScript>().tileMapWall.SetActive(false);
            }
        }
    }
    
    
    public void Attack1()
    {
        Debug.Log("Attack1");
    }

    public void Attack2()
    {
        Debug.Log("Attack2");
    }
    
    public IEnumerator Attack1Duration()
    {
        onAttack = true;
        onAttack1 = true;
        yield return new WaitForSeconds(4f);
        onAttack1 = false;
        onAttack = false;
    }

    public IEnumerator Attack2Duration()
    {
        onAttack = true;
        yield return new WaitForSeconds(2f);
        onAttack2 = true;
        yield return new WaitForSeconds(3.5f);

        onAttack2 = false;
        onAttack = false;
    }
}