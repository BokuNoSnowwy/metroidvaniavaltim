﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Configuration;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlantBossScript : MonoBehaviour
{
    public bool awake = false;
    public bool p2 = false;
    public monsterScript stats;
    public float baseHealth;
    public GameObject player;

    public GameObject firePrefab1;
    public GameObject firePrefab2;

    public Transform[] fireSpots;

    public float timerAttack = 3;
    public float timerAttackReset = 3;

    public GameObject bonusLoot;
    
    //Animator
    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        stats = GetComponent<monsterScript>();
        baseHealth = stats.health;
    }

    // Update is called once per frame
    void Update()
    {
        if (stats.health <= baseHealth / 2)
        {
            p2 = true;
            animator.SetBool("MidLife",true);
            timerAttackReset = 2;
        }
        if (awake)
        {
            animator.SetBool("Awake", true);
            
            if (timerAttack >= 0)
            {
                timerAttack -= Time.deltaTime;        
            }
            if (timerAttack <= 0)
            {
                if (p2)
                {
                    Attack2();
                }
                else
                {
                    int attack = Random.Range(0, 2);
                    switch (attack)
                    {
                        case 0 :
                            Attack1(fireSpots[0].transform);
                            break;
                        case 1 :
                            Attack1(fireSpots[1].transform);
                            break;
                        default:
                            break;
                    }
                }
               
                timerAttack = timerAttackReset;
            }
        }

        if (stats.health <= 0)
        {
            FindObjectOfType<Audiomanager>().Stop("MainAudio");
            FindObjectOfType<Audiomanager>().Play("MusicMain2");
            Instantiate(bonusLoot, transform.position,Quaternion.identity);
            gameObject.SetActive(false);
            GetComponentInChildren<PlantBossDetectionScript>().tileMapWall.SetActive(false);
        }
    }

    public void Attack1(Transform spot)
    {
        for (int i = 0; i < 6; i++)
        {
            float inter = 36f;
            Instantiate(firePrefab1, spot.position, Quaternion.Euler(spot.rotation.x, spot.rotation.y, 90f + inter * i));
        }
    }

    public void Attack2()
    {
        Instantiate(firePrefab2, new Vector3(player.transform.position.x + 4.5f,player.transform.position.y-0.5f,player.transform.position.z), Quaternion.Euler(new Vector3(0f,0f,0f)));
        Instantiate(firePrefab2, new Vector3(player.transform.position.x - 4.5f,player.transform.position.y-0.5f,player.transform.position.z), Quaternion.Euler(new Vector3(0f,180f,0f)));
    }

}
