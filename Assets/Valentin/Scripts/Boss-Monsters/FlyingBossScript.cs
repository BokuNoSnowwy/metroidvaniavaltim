﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Configuration;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class FlyingBossScript : MonoBehaviour
{
    public bool awake = false;
    public bool onAttack = false;
    public bool onAttack1 = false;
    public bool onAttack2 = false;
    public bool onAttack3 = false;

    public GameObject player;

    private float baseHealth;
    public monsterScript stats;
    private bool isDamaged = false;


    public GameObject firePrefab1;
    public GameObject firePrefab2;

    public Transform[] fireSpotsWings;
    public Transform fireSpotButt;
    
    public float timerAttack = 6;
    public float timerAttackReset = 6;

    public float timerAttack1 = 0.3f;
    
    public float timerAttack2 = 0.3f;
    
    //oscillation
    private float timeCounter = 0;
    public float speedMove;
    public float width;
    public float height;
    
    //Loot power
    public GameObject bonusLoot;
    
    //animator
    public Animator animator;


    // Start is called before the first frame update
    void Start()
    {
        stats = GetComponent<monsterScript>();
        baseHealth = stats.health;
    }

    // Update is called once per frame
    void Update()
    {
        if (awake)
        {
            if (isDamaged)
            {
                animator.SetBool("Awake",false);
                animator.SetBool("Damaged", true);
            }
            else
            {
                animator.SetBool("Damaged", false);
                animator.SetBool("Awake",true);
            }
            
            
            if (!onAttack)
            {
                //Movements
                timeCounter += Time.deltaTime * speedMove;

                float x = 15.16763f + Mathf.Cos(timeCounter)*width;
                float y = -62.7f + Mathf.Sin(timeCounter)*height;
                float z = 4.4f + 0;
                
                transform.position = new Vector3(x,y,z);
                
                if (timerAttack >= 0)
                {
                    timerAttack -= Time.deltaTime;        
                }

                if (timerAttack <= 0)
                {
                    int attack = Random.Range(0, 2);
                    switch (attack)
                    {
                        case 0 :
                            StartCoroutine(Attack1Duration());
                            break;
                        case 1 :
                            StartCoroutine(Attack2Duration(x,y));
                            break;
                        default:
                            break;
                    }
                    timerAttack = timerAttackReset;
                }
            }
            else
            {
                if (onAttack1)
                {
                    
                    if (timerAttack1 >= 0)
                    {
                        timerAttack1 -= Time.deltaTime;
                    }

                    if (timerAttack1 <= 0)
                    {
                        Attack1();
                        timerAttack1 =  Random.Range(0.1f,0.3f);
                    }
                }
                
                if (onAttack2)
                {
                    
                    if (timerAttack2 >= 0)
                    {
                        timerAttack2 -= Time.deltaTime;
                    }

                    if (timerAttack2 <= 0)
                    {
                        Attack2();
                        timerAttack2 =  Random.Range(0.1f,0.15f);
                    }
                }
            }
            
        }
        
        if (stats.health <= 0)
        {
            FindObjectOfType<Audiomanager>().Stop("MusicMain2");
            FindObjectOfType<Audiomanager>().Play("MusicMain3");
            Instantiate(bonusLoot, transform.position,Quaternion.identity);
            gameObject.SetActive(false);
            GetComponentInParent<FlyingBossDetection>().tileMapWall.SetActive(false);
        }
    }

    public IEnumerator DamagedAnimation()
    {
        isDamaged = true;
        yield return new WaitForSeconds(1f);
        isDamaged = false;
    }
    
    public IEnumerator Attack1Duration()
    {
        onAttack = true;
        onAttack1 = true;
        yield return new WaitForSeconds(3.5f);
        onAttack1 = false;
        onAttack = false;
    }

    public IEnumerator Attack2Duration(float x, float y)
    {
        onAttack = true;
        transform.DOMove(new Vector2(player.transform.position.x, player.transform.position.y + 5f),1f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(2f);
        onAttack2 = true;
        yield return new WaitForSeconds(3.5f);

        onAttack2 = false;
        onAttack = false;
    }
    

    public void Attack1()
    {
        for (int j = 0; j < 4; j++)
            {
                int randomAngle = Random.Range(-20, 20);

                Instantiate(firePrefab1, fireSpotsWings[0].position,
                    Quaternion.Euler(fireSpotsWings[0].transform.rotation.x, fireSpotsWings[0].transform.rotation.y,
                        fireSpotsWings[0].transform.rotation.z + randomAngle));
                
                Instantiate(firePrefab1, fireSpotsWings[1].position,
                    Quaternion.Euler(fireSpotsWings[1].transform.rotation.x, fireSpotsWings[1].transform.rotation.y,
                        fireSpotsWings[1].transform.rotation.z + randomAngle + 90f));
                //Instantiate(firePrefab1, fireSpotsWings[i].position, gameObject.transform.rotation);
            }
    }

    public void Attack2()
    {
        for (int i = 0; i < 10; i++)
        {
            int randomAngle = Random.Range(30, 70);

            Instantiate(firePrefab2, fireSpotButt.position,
                Quaternion.Euler(fireSpotButt.transform.rotation.x, fireSpotButt.transform.rotation.y,
                    randomAngle));
        }
    }
}
