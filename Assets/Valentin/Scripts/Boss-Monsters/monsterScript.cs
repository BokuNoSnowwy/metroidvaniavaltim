﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using DG.Tweening;
using UnityEngine.Experimental.PlayerLoop;


public class monsterScript : MonoBehaviour
{
    
   // public BoxCollider2D boxCollider;
   // public CapsuleCollider2D capsuleCollider1;
   // public CapsuleCollider2D capsuleCollider2;
   
   public List<string> qteList = new List<string>();
    public int nbKey;
    public int initialKey;
    public string stringElement = "az";

    public float health;
    public int damage;
    public bool canInflictDmg = true;

    private bool isAddforce;
    private GameObject player;
    public Rigidbody2D rgbd;

    public float knockback = 3;
    private float knockbackLenght;
    public float knockbackCount;

    private Patrolmonstre monster;
    public FloatingNumbers dmgText;
    public GameObject prefabHeart;

    // Start is called before the first frame update
    void Start()
    {
        rgbd = GetComponent<Rigidbody2D>();
        dmgText = GameObject.FindWithTag("DmgText").GetComponent<FloatingNumbers>();
        player = GameObject.FindGameObjectWithTag("Player");
        if (GetComponent<Patrolmonstre>())
        {
            monster = GetComponent<Patrolmonstre>();
        }
        
        initialKey = nbKey;
        for (int i = 0; i < nbKey; i++)
        {
            int random = Random.Range(0, stringElement.Length);
            qteList.Add(stringElement[random].ToString());
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Si le monstre n'est pas un boss.
        if (gameObject.tag == "Monster")
        {
            if (knockbackCount <= 0)
            {
                canInflictDmg = true;
                if (monster)
                {
                    monster.knockback = false;
                }
            }
            else
            {
                Debug.Log("KnockBack");
                canInflictDmg = false;
                if ((player.transform.position.x - gameObject.transform.position.x) >= 0f)
                {
                    rgbd.velocity = new Vector2(-knockback,0);
                }
                else
                {
                    rgbd.velocity = new Vector2(knockback,0);
                }

                if (monster)
                {
                    monster.knockback = true;
                }
                knockbackCount -= Time.deltaTime;
            }
        }
    }
    
    public void RefreshList(int newNbKey)
    {
        //Debug.Log("RefreshList");
        nbKey = newNbKey;
        qteList.Clear();
        for (int i = 0; i < newNbKey; i++)
        {
            int random = Random.Range(0, stringElement.Length);
            qteList.Add(stringElement[random].ToString());
        }
    }

    public void TakeDamage(float damage,Transform playerPosition)
    {
<<<<<<< HEAD:Assets/Valentin/Scripts/monsterScript.cs
        health -= damage;
        canInflictDmg = false;
        FeedBackDamage();
        knockbackCount = 1f;

=======
        if (dmgText)
        {
            dmgText.damageNumber = (int) damage;
            dmgText.displayDamage = true;
        }
        
        if (gameObject.CompareTag("Boss"))
        {
            if (GetComponent<FlyingBossScript>())
            {
                StartCoroutine(GetComponent<FlyingBossScript>().DamagedAnimation());
            }
        }
        
        
        health -= damage;
        FeedBackDamage();
        knockbackCount = 0.75f;
>>>>>>> master:Assets/Valentin/Scripts/Boss-Monsters/monsterScript.cs
        if (health <= 0)
        {
            FindObjectOfType<Audiomanager>().Play("MonsterDie");
            if (gameObject.CompareTag("Boss"))
            {
                
            }
            else
            {
                int random = Random.Range(1, 6);
                if (random == 2)
                {
                    Instantiate(prefabHeart, transform.position, Quaternion.identity);
                }
                gameObject.SetActive(false);
            }
            
        }
    }

    public IEnumerator WaitForQTE()
    {
        foreach (var VARIABLE in GetComponents<Collider2D>())
        {
            VARIABLE.enabled = false;
        }
        yield return new WaitForSeconds(1f);
        foreach (var VARIABLE in GetComponents<Collider2D>())
        {
            VARIABLE.enabled = true;
        }
    }

    public void FeedBackDamage()
    {
        GetComponent<SpriteRenderer>().DOColor(Color.red, 0.1f)
            .OnComplete(()=>GetComponent<SpriteRenderer>().DOColor(Color.white, 0.1f)
                .OnComplete(()=>GetComponent<SpriteRenderer>().DOColor(Color.red, 0.1f)
                .OnComplete(()=>GetComponent<SpriteRenderer>().DOColor(Color.white, 0.1f)
                    .OnComplete(()=>GetComponent<SpriteRenderer>().DOColor(Color.red, 0.1f)
                        .OnComplete(()=>GetComponent<SpriteRenderer>().DOColor(Color.white, 0.1f))))));
    }
}
