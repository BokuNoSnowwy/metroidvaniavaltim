﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TMPro;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class FloatingNumbers : MonoBehaviour
{
    public float moveSpeed;

    public int damageNumber;

    public TextMeshProUGUI displayNumber;

    public bool displayDamage;

    public float displayDamageTimer = 1;
    private float displayDamageTimerReset = 1;

    public PlayerMovements player;
    public SwordScript sword;

    public float fontSize;
    public float fontSizeMultiplierQTE;
    public float distanceTxtDmgFromPlayer;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player").GetComponent<PlayerMovements>();
        sword = player.sword.GetComponentInChildren<SwordScript>();
    }

    // Update is called once per frame
    void Update()
    {
        byte color;
        switch (sword.chainQTE)
        {
            case 1:
                color = 255;
                break;
            case 2:
                color = 155;
                break;
            case 3:
                color = 75;
                break;
            case 4:
                color = 25;
                break;
            default:
                color = 0;
                break;
        }

        if (displayDamage)
        {
            //Grossis le texte et le met de plus en plus rouge en fonction de la chaine de coups réussis
            displayNumber.fontSize = fontSize+sword.chainQTE * fontSizeMultiplierQTE;
            displayNumber.faceColor = new Color32(255,color,color,255);

            displayDamageTimer -= Time.deltaTime;
            displayNumber.text = "" + damageNumber;
            displayNumber.transform.position = new Vector3(displayNumber.transform.position.x,displayNumber.transform.position.y + (moveSpeed*Time.deltaTime),displayNumber.transform.position.z);
            
            if (displayDamageTimer <= 0)
            {
                displayDamage = false;
                displayDamageTimer = displayDamageTimerReset;
            }
        }
        else
        {
            displayNumber.text = "";
            if (player.transform.rotation.y == 0)
            {
                displayNumber.transform.position = new Vector3(player.transform.position.x + distanceTxtDmgFromPlayer,player.transform.position.y,player.transform.position.z);
            }
            else
            {
                displayNumber.transform.position = new Vector3(player.transform.position.x - distanceTxtDmgFromPlayer,player.transform.position.y,player.transform.position.z);
            }
        }
    }
}
