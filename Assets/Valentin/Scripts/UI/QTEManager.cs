﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class QTEManager : MonoBehaviour
{
    public monsterScript monster;
    public TimeManagerScript timeManager;
    public SwordScript playerSword;
    
    public bool onQte = false;
    public TextMeshProUGUI textQTE;
    public bool qteIsDelayed = false;
    
    public int indexList;
    public List<string> currentList = new List<string>();

    public float timerQte = 2;
    public float resetTimeQte = 2;
    
    // Start is called before the first frame update
    void Start()
    {
        playerSword = GameObject.FindObjectOfType<SwordScript>();
    }

    // Update is called once per frame
    void Update()
    {

        /*
        //TODO Demander à clement le problem
        if (timerQte == 2 && playerSword.focusedMonster == null)
        {
            playerSword.inMonster = false;
        }
        */
        if (onQte)
        {
            //Timer du QTE qui n'est pas affecté par le ralentissement du temps.
            if (timerQte >= 0)
                {
                    timerQte -= Time.unscaledDeltaTime;
                    //monster.GetComponent<CircleCollider2D>().enabled = false;
                }
            if (timerQte <= 0)
            {
                monster.RefreshList(monster.initialKey);
                DesactivateQTE(monster);
                timerQte = 2;
                onQte = false;
                //playerSword.inMonster = false;
            }
           

            if (monster)
            {
                if (currentList.Count >= 1)
                {
                    if (Input.GetKeyDown(KeyCode.A))
                    {
                        //Success
                        if (currentList[0].ToUpper() == "A")
                        {
                            currentList.RemoveAt(indexList);
                            RefreshListTextMesh(currentList);
                        }
                        //Fail
                        else
                        {
                            DesactivateQTE(monster);
                            onQte = false;
                        }
                    }
                
                    if (Input.GetKeyDown(KeyCode.Z))
                    {
                        //Success
                        if (currentList[0].ToUpper() == "Z")
                        {
                            currentList.RemoveAt(indexList);
                            //Met à jour le text mesh
                            RefreshListTextMesh(currentList);
                        }
                        //Fail
                        else
                        {
                            DesactivateQTE(monster);
                            onQte = false;
                        }
                    }
                    
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        //Success
                        if (currentList[0].ToUpper() == "E")
                        {
                            currentList.RemoveAt(indexList);
                            RefreshListTextMesh(currentList);
                        }
                        //Fail
                        else
                        {
                            DesactivateQTE(monster);
                            onQte = false;
                        }
                    }
                }
                else if (currentList.Count <= 0)
                {
                    DesactivateQTE(monster);
                }
                if (!textQTE.IsActive())
                {
                    textQTE.enabled = true;
                }
            }
        }
        else
        {
            timerQte = resetTimeQte;
            textQTE.enabled = false;
        }
    }

    public void ActivateQTE(monsterScript monsterDamaged)
    {
        if (!onQte)
        {
            monster = monsterDamaged;
            
            onQte = true;
            currentList = monsterDamaged.qteList;
            indexList = 0;
            RefreshListTextMesh(currentList);
        }
    }
    
    public void DesactivateQTE(monsterScript monsterDamaged)
    {
        if (onQte)
        {
            if (currentList.Count < 1)
            {
                //Debug.Log("Qte is win, chain +1");
                monsterDamaged.nbKey += 1;
                monsterDamaged.RefreshList(monsterDamaged.nbKey);
                monsterDamaged.TakeDamage(playerSword.swordDmg * (1+playerSword.chainQTE*0.1f),playerSword.player.transform);
                playerSword.chainQTE += 1;
                timeManager.ReturnNormalTime();
            }
            else
            {
                //Debug.Log("Qte is lose, chain is reset");
                monsterDamaged.RefreshList(monsterDamaged.initialKey);
                playerSword.chainQTE = 1;
            }
        }
        playerSword.inMonster = false;
        textQTE.text = "";
        onQte = false;
        currentList = null;
        monster = null;
        timeManager.ReturnNormalTime();

    }

    public void RefreshListTextMesh(List<string> list)
    {
        textQTE.text = "";
        foreach (var element in list)
        {
            textQTE.text += element;
        }
    }
}
