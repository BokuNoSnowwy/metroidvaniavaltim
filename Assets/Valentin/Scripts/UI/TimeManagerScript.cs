﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManagerScript : MonoBehaviour
{
    public float slowdownLenght;
    public float slowdownFactor = 0.05f;
    public float timerSlowDown = 0.01f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Time.timeScale += (1f / slowdownLenght) * Time.unscaledDeltaTime;
        Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);
    }

    //TODO Transformer en Routine qui prend en parametre une valeur, valeur étant le temps du bullet time.
    public void BulletTime()
    {
        slowdownLenght = 10f;
        Time.timeScale = slowdownFactor;
        Time.fixedDeltaTime = Time.timeScale * timerSlowDown;
    }

    public void ReturnNormalTime()
    {
        slowdownLenght = 0.1f;
    }
}
