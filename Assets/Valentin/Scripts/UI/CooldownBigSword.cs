﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CooldownBigSword : MonoBehaviour
{

    public GameObject imageCooldownSword;
    public TextMeshProUGUI cooldownText;
    public GameObject panelCooldown;
    public TextMeshProUGUI pressAText;

    public PlayerMovements player;
    public PlayerPowers playerPowers;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovements>();
        playerPowers = player.GetComponent<PlayerPowers>();
        
        imageCooldownSword.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //TODO L'alpha ne change pas A CORRIGER
        if (!player)
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovements>();
        }
        else
        {
            var color = panelCooldown.GetComponent<Image>().color;
            
            if (player.GetComponent<PlayerPowers>().onPowerBigSword)
            {
                imageCooldownSword.SetActive(true);
                if (playerPowers.cooldownBigSword > 0)
                {
                    pressAText.gameObject.SetActive(false);
                    
                    if (player.GetComponentInChildren<SwordScript>().onBigSword)
                    {
                        color.a = 150f;
                        cooldownText.text = "On Use";
                    }
                    else
                    {
                        int cooldownInt = (int)playerPowers.cooldownBigSword;
                        cooldownText.text = cooldownInt.ToString();
                        
                    }
                }
                else
                {
                    cooldownText.text = "";
                    color.a = 155;
                    pressAText.gameObject.SetActive(true);
                }
                
            }
            else
            {
                imageCooldownSword.SetActive(false);
            }
        }
        
    }
}
