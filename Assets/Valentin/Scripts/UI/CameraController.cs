﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    public Vector3 distance;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        //calcul la distance entre le player et la caméra

        distance = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!player)
        {

        }
        else
        {
           
            //La caméra va suivre le player en prennant en compte la distance initialement calculée
            transform.position = player.transform.position + distance;
        }
    }

    public void newTargetPlayer(GameObject newPlayer)
    {
        player = newPlayer;
    }
}
