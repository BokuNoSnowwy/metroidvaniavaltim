﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TpScript : MonoBehaviour
{
    public GameObject[] listTp;
    public GameObject[] listTpCanvas;
    public int currentIndex;
    public GameObject tpCanvas;

    public bool onTp = false;
    public GameObject player;
    
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
        tpCanvas.SetActive(false);
        
        //désactive les tp quand le jeu se lance
        foreach (var tp in listTp)
        {
            tp.GetComponent<TpCollider>().activated = false;
        }

        foreach (var tp in listTpCanvas)
        {
            tp.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Quand le joueur est sur le panel de tp, ce dernier affiche les tp qu'il a déja activé en passant dessus
        if (onTp)
        {
            tpCanvas.SetActive(true);
            //Récupère la liste des tp désactivés et désactive les images dans le canvas correspondant a l'index des tp non activés
            foreach (var tpSpot in listTp)
            {
                if (tpSpot.GetComponent<TpCollider>().activated)
                {
                    listTpCanvas[tpSpot.GetComponent<TpCollider>().index].gameObject.SetActive(true);
                }
            }
        }
        else
        {
            tpCanvas.SetActive(false);
        }
        
        
    }

    //Quand le joueur sélectionne une image(tp) le téléporte à l'emplacement indiqué
    public void TpWithImage(ImageTp image)
    {
        Debug.Log(image.index);
        StartCoroutine(TpPlayer(image.index));
    }

    //Fonction pour téléporter le joueur sur un TP
    public IEnumerator TpPlayer(int index)
    {
        onTp = false;
        yield return new WaitForSeconds(1);
        player.transform.position = listTp[index].transform.position;
    }
}
