﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Win32;
using UnityEngine;

public class BigSwordBonus : MonoBehaviour
{
    private PlayerStats heal;
    public int healthpoint = 50;
    private bool onDestroy = false;
    public ParticleSystem particlePickUp;
    // Start is called before the first frame update
    void Start()
    {
        heal = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {  
            FindObjectOfType<Audiomanager>().Play("HeartRecup");
            heal.CurrentHealth += healthpoint;
            heal.healthbar.value = heal.CalculateHealth();
            if (heal.CurrentHealth > heal.Maxhealth)
            {
                heal.CurrentHealth = heal.Maxhealth;
            }
            PickUpPower(other);
        }
    }

    private void PickUpPower(Collider2D player)
    {
        //TODO 
        player.GetComponent<PlayerPowers>().onPowerBigSword = true;
        
        StartCoroutine(GameObject.FindWithTag("InfoText").GetComponent<InfoTextDisplay>().DisplayPowerBigSword());

        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
        GetComponent<CircleCollider2D>().enabled = false;
        
        Destroy(gameObject.GetComponentInChildren<ParticleSystem>());
        onDestroy = true;
    }
}
