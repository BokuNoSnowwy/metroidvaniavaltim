﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Configuration;
using UnityEngine;
using DG.Tweening;
using UnityEditor.UIElements;

public class mobShoot : MonoBehaviour
{
    public GameObject bullet;
    private Rigidbody2D rgbd;
    public float timeBtwshots;
    public Transform player;
    private bool lookat = false;

    public float startshoot;

    public bool fireon = false;
    // Start is called before the first frame update
    void Start()
    {
        rgbd = this.GetComponent<Rigidbody2D>();
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            lookat = true;
            fireon = true;
        }

    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            lookat = false;
            fireon = false;
        }

    }

    // Update is called once per frame
    void Update()
    {
        // Permet au mob de tirer des bullet
        if (fireon)
        {
            if (timeBtwshots <= 0)
            {
                Instantiate(bullet, transform.position, Quaternion.identity);
                timeBtwshots = startshoot;
            }
            else
            {
                timeBtwshots -= Time.deltaTime;
            }

        }

        if (lookat)
        { // Permet au monstre de rotate pour pouvoir tirer vers le joueur
            if (player.transform.position.x > transform.position.x) 
            {  
                transform.parent.localRotation = Quaternion.Euler(0f, -180f, 0f);
            
            }

            else if (player.transform.position.x < transform.position.x)
            {
                
                transform.parent.localRotation = Quaternion.Euler(0f, 0f, 0f);
            }

        }
    }
}