﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mooveplayerPLATFORM : MonoBehaviour
{
    
    private Transform platform;
    public Transform player;
    // Start is called before the first frame update
    void Start()
    {
        platform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
// le player deviens enfant des platform et suis leurs mouvements
    public void OnTriggerEnter2D(Collider2D other1)
    {
        if (other1.gameObject.CompareTag("Player"))
        {
            player.transform.SetParent(platform.transform);
        }
        
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            player.transform.SetParent(null);
        }
      
    }
}
