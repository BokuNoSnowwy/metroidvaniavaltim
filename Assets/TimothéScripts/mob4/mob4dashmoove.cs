﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class mob4dashmoove : MonoBehaviour
{
    private Rigidbody2D rb;
    private Transform mob;
    public float dashspeed;
    public Transform player;
    private float dashtime;
    public bool dashing = false;
    public float startDashtime;
    public bool lookat = false;
    
    public SpriteRenderer swordEnnemi;
   
    private PlayerMovements playermove;

  
    // Start is called before the first frame update
    void Start()
    {
       
        rb = GetComponentInParent<Rigidbody2D>();
        dashtime = startDashtime;
        mob = GetComponentInParent<Transform>();
        playermove = FindObjectOfType<PlayerMovements>();
    }

    IEnumerator DashonPlayerL()
    {// lance une coroutine qui permet de faire bouger le monstre avec un "dash" coter gauche
        
            DashisComing();
            lookat = true; 
            yield return new WaitForSeconds(2.0f);
            rb.velocity = Vector2.left * dashspeed;
            dashing = true;
            yield return new WaitForSeconds(0.5f);
            lookat = false;
            rb.velocity = Vector2.zero;
            yield return  new WaitForSeconds(1f);
        
       
        
    }

    IEnumerator DashOnplayeR()
    {// lance une coroutine qui permet de faire bouger le monstre avec un "dash" coter droit
        
            DashisComing();
            lookat = true;
            yield return new  WaitForSeconds(2.0f);
            rb.velocity = Vector2.right * dashspeed;
            dashing = true;
            yield return  new WaitForSeconds(0.5f);
            lookat = false;
            rb.velocity = Vector2.zero;
            yield return new WaitForSeconds(1f);
        
       
       
    }

    public void OnTriggerEnter2D(Collider2D other)
    {// le mob4 va dasher vers le player quand il rentre dans son trigger avec une coroutine
        if (other.gameObject.CompareTag("Player") && player.transform.position.x < mob.parent.transform.position.x && playermove.isGrounded)
        {
            StartCoroutine(DashonPlayerL());
         
           
        }

        else if (other.gameObject.CompareTag("Player") && player.transform.position.x > mob.parent.transform.position.x && playermove.isGrounded)
        {
          
            StartCoroutine(DashOnplayeR());
            
            
        }

        
    }

    public void OnTriggerExit2D(Collider2D other)
    {// Le monstre ne regarde plus le joueur quand il sort de sa zone d'aggro
        if (other.gameObject.CompareTag("Player"))
        {
            lookat = false;
          
        }
    }


    // Update is called once per frame
    void Update()
    {
       /* if (dashing == true)
        {
            dashtime -= Time.deltaTime;
            if (dashtime <= 0)
            {
                dashtime = startDashtime;
                
                dashing = false;
            }
        }*/

        if (lookat == true)
        {//Permet au monstre de se tourner vers le joueur
            if (player.transform.position.x > mob.parent.transform.position.x)
            {
                mob.parent.transform.localRotation =  Quaternion.Euler(0f, -180f, 0f);
            }
            else if (player.transform.position.x < mob.parent.transform.position.x)
            {
                mob.parent.transform.localRotation =  Quaternion.Euler(0f, 0f, 0f);
            }
            
        }
       
        
    }
    
    public void DashisComing()
    {// Sert a prévenir le joueur que l'ennemi va dasher
        swordEnnemi.DOColor(Color.red, 1.5f)
            .OnComplete(() => swordEnnemi.DOColor(Color.white, 1f));

    }
    
}
