﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerCollider : MonoBehaviour
{
    public float ejection;
    public LayerMask layers;
    public Rigidbody2D rgbd;
    public float distance;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {// Permet d'empecher le player de rester coincer dans les murs
        
        RaycastHit2D wallrightInfo = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - 0.5f), Vector2.right, distance, layers);
        RaycastHit2D WallleftInfo = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - 0.5f), Vector2.left, distance, layers);

        Debug.DrawRay(new Vector2(transform.position.x, transform.position.y - 0.5f), Vector2.right, Color.red, distance);
        Debug.DrawRay(new Vector2(transform.position.x, transform.position.y - 0.5f), Vector2.left, Color.blue, distance);
        
        if (wallrightInfo == true && Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.LeftArrow) && rgbd.velocity.y > 0 )
        {
            rgbd.velocity += Vector2.up * Physics2D.gravity.y * Time.deltaTime;
            rgbd.velocity += Vector2.left * ejection * Time.deltaTime;
        }

        if (WallleftInfo == true && (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.RightArrow) && rgbd.velocity.y > 0))
        {
            rgbd.velocity += Vector2.up * Physics2D.gravity.y * Time.deltaTime;
            rgbd.velocity += Vector2.right * ejection * Time.deltaTime;

        }
    }
}
