﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TreeEditor;
using UnityEngine;

public class PlatformeMoovLeftToRight : MonoBehaviour
{
    // Start is called before the first frame update
    public  void Start()
    {
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(transform.DOMoveX(transform.position.x + 4, 3));
        mySequence.AppendCallback(Reloop);


    }

    public void Reloop()
    {
        Sequence mySequence1 = DOTween.Sequence();
        mySequence1.Append(transform.DOMoveX(transform.position.x - 4, 3));
        mySequence1.AppendCallback(Start);
    }

    // Update is called once per frame
    void Update()
    {
  
    }

  
}
