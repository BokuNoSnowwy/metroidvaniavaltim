﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class panelPause : MonoBehaviour
{
   
    public GameObject panel;
    private bool paneltrue = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {//     Active le menu pause et le désactive
        if (Input.GetKeyDown(KeyCode.Escape) && paneltrue == false)
        {
            panel.SetActive(true);
          
            paneltrue = true;
            Time.timeScale = 0f;
           
        }else if (Input.GetButtonDown("Cancel") && paneltrue == true)
        {
            Time.timeScale = 1f;
            panel.SetActive(false);
           
            paneltrue = false;
        }
        
    }
}
