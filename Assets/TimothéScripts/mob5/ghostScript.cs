﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ghostScript : MonoBehaviour
{
   
    public float speed;
    public Transform player;
    
   

    public bool follow = false;
    public GameObject monsterG;
 
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        
    }

    // Update is called once per frame
    void Update()
    {// Follow le player
        Vector2 target = new Vector2(player.transform.position.x, player.position.y);
        if (follow)
        {
            monsterG.transform.position = Vector2.MoveTowards(monsterG.transform.position, target, speed * Time.deltaTime);
        }
       
     
        
    }

   public void OnTriggerEnter2D(Collider2D other)
    {
        

        if (other.gameObject.CompareTag("Player"))
        {
            follow = true;
        }
        
    }

 
}
